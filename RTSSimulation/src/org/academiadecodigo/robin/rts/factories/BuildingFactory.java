package org.academiadecodigo.robin.rts.factories;

import org.academiadecodigo.robin.rts.building.Barracks;
import org.academiadecodigo.robin.rts.building.Building;
import org.academiadecodigo.robin.rts.building.BuildingType;
import org.academiadecodigo.robin.rts.building.StrongHold;

public class BuildingFactory {

    public static Building createBuilding(BuildingType buildingType){

        Building building = null;

        switch (buildingType){
            case BARRACKS:
                building = new Barracks();
                break;

            case STRONGHOLD:
                building = new StrongHold();
                break;
        }

        return building;

    }
}