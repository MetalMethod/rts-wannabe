package org.academiadecodigo.robin.rts.factories;

import org.academiadecodigo.robin.rts.unit.*;
import org.academiadecodigo.robin.rts.unit.manaunits.Wizard;

public class UnitFactory {

    public static Unit createUnit(UnitType unitType){

        Unit unit = null;

        switch (unitType){

            case ARCHER:
                unit = new Archer();
                break;

            case KNIGHT:
                unit = new Knight();
                break;

            case WIZARD:
                unit = new Wizard();
                break;

            case WORKER:
                unit = new Worker();
                break;

            case FOOTMAN:
                unit = new Footman();
                break;
        }

        return unit;
    }
}
