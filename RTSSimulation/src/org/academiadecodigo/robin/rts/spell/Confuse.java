package org.academiadecodigo.robin.rts.spell;

import org.academiadecodigo.robin.rts.unit.Unit;

public class Confuse extends Spell {


    @Override
    public void cast(Unit unit, int spellDamageMultiplier) {

        unit.suffer(spellDamageMultiplier);
    }
}
