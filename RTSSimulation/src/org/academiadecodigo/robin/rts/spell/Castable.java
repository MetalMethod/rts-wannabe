package org.academiadecodigo.robin.rts.spell;

import org.academiadecodigo.robin.rts.unit.Unit;

public interface Castable {

    void cast(Unit unit, int spellDamageModifier);
}
