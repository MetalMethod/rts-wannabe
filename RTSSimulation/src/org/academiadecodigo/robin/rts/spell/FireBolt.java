package org.academiadecodigo.robin.rts.spell;

import org.academiadecodigo.robin.rts.unit.Unit;

public class FireBolt extends Spell {


    @Override
    public void cast(Unit unit, int spellDamageModifier) {
        unit.suffer(spellDamageModifier);
    }
}
