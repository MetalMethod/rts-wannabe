package org.academiadecodigo.robin.rts.resources;

public class Tree extends Resource {

    private static final int MAX_WOOD = 60;

    public Tree(){
        currentAmount = MAX_WOOD;
    }
}
