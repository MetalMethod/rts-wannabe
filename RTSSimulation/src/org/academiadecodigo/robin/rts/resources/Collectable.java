package org.academiadecodigo.robin.rts.resources;

public interface Collectable {

    int collect(int amountCollected);
}
