package org.academiadecodigo.robin.rts.damageInterfaces;

public interface Offensive {

    void attack(Hittable hittable);
}
