package org.academiadecodigo.robin.rts;

import org.academiadecodigo.robin.rts.building.Building;
import org.academiadecodigo.robin.rts.building.StrongHold;
import org.academiadecodigo.robin.rts.unit.Unit;
import org.academiadecodigo.robin.rts.unit.Worker;

import java.util.LinkedList;

public class Player {

    public static final int STARTING_WORKERS_NUMBER = 5;
    public static final int MAX_ARMY_SIZE = 100;

    private int gold;
    private int wood;
    private LinkedList<Unit> army;
    private LinkedList<Building> buildings;

    public Player(){
        army = new LinkedList<>();
        buildings = new LinkedList<>();
        gold = 0;
        wood = 0;
        init();
    }

    private void init(){

        for (int i = 0; i < STARTING_WORKERS_NUMBER ; i++) {
            army.add(new Worker());
        }

        buildings.add(new StrongHold());
    }

}
