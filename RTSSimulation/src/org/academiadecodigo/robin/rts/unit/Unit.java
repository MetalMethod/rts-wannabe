package org.academiadecodigo.robin.rts.unit;

import org.academiadecodigo.robin.rts.damageInterfaces.Hittable;
import org.academiadecodigo.robin.rts.damageInterfaces.Offensive;

public abstract class Unit implements Offensive, Hittable {

    protected int maxHealth;
    protected int currentHealth;
    protected int damage;

    public Unit(int maxHealth, int damage) {
        this.maxHealth = maxHealth;
        currentHealth = maxHealth;
        this.damage = damage;

    }

    @Override
    public void suffer(int damage){
        currentHealth -= damage;
    }

    @Override
    public void attack(Hittable hittable) {
        hittable.suffer(damage);
    }
}
