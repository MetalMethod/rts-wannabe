package org.academiadecodigo.robin.rts.unit.manaunits;

import org.academiadecodigo.robin.rts.spell.Confuse;
import org.academiadecodigo.robin.rts.spell.FireBolt;
import org.academiadecodigo.robin.rts.spell.Spell;
import org.academiadecodigo.robin.rts.spell.SpellType;
import org.academiadecodigo.robin.rts.unit.Unit;

import java.util.HashMap;

public class Wizard extends SpellUsingUnit {


    public Wizard() {

        super(100,10,2);
        spellBook = assembleSpellBook();
    }

    @Override
    public void castSpell(SpellType spellType, Unit unit) {

        spellBook.get(spellType).cast(unit,spellDamage);
    }

    @Override
    public HashMap<SpellType, Spell> assembleSpellBook() {

        HashMap<SpellType, Spell> spellBook = new HashMap<>();

        spellBook.put(SpellType.FIREBOLT, new FireBolt());
        spellBook.put(SpellType.CONFUSE, new Confuse());


        return spellBook;

    }
}
