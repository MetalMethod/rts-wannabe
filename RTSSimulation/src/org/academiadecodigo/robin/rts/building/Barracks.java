package org.academiadecodigo.robin.rts.building;

import org.academiadecodigo.robin.rts.factories.UnitFactory;
import org.academiadecodigo.robin.rts.unit.Unit;
import org.academiadecodigo.robin.rts.unit.UnitType;


public class Barracks extends Building {


    public Barracks(){
        super(800);
    }


    public Unit trainFootman(){
        return UnitFactory.createUnit(UnitType.FOOTMAN);
    }

    public Unit trainKnight(){
        return UnitFactory.createUnit(UnitType.KNIGHT);
    }

}
