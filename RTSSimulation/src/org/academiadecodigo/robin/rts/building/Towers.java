package org.academiadecodigo.robin.rts.building;

import org.academiadecodigo.robin.rts.damageInterfaces.Hittable;
import org.academiadecodigo.robin.rts.damageInterfaces.Offensive;

public class Towers extends Building implements Offensive {

    private int damage;

    public Towers() {
        super(300);
        damage = 30;
    }

    @Override
    public void attack(Hittable hittable) {
        hittable.suffer(damage);
    }
}
