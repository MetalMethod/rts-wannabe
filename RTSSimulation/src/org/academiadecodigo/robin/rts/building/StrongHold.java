package org.academiadecodigo.robin.rts.building;

import org.academiadecodigo.robin.rts.factories.UnitFactory;
import org.academiadecodigo.robin.rts.unit.Unit;
import org.academiadecodigo.robin.rts.unit.UnitType;

public class StrongHold extends Building{

    public StrongHold(){
        super(1200);
    }


    public Unit trainWorker() {
        return UnitFactory.createUnit(UnitType.WORKER);
    }

}
