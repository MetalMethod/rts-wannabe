package org.academiadecodigo.robin.rts.building;

import org.academiadecodigo.robin.rts.damageInterfaces.Hittable;

public abstract class Building implements Hittable {

    protected int durability;

    public Building(int durability){
        this.durability = durability;
    }

    @Override
    public void suffer(int damage) {
        durability -= damage;
    }
}
